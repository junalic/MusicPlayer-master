package com.goodjob.musicplayer.util;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Author by sxjun.he, Date on 21-4-2.
 * PS: Not easy to write code, please indicate.
 */
public class ConvertFileCode {
    public String converfile(String filepath){
        System.out.println("ConvertFileCode--------->"+filepath);
        File file=new File(filepath);
        FileInputStream fis=null;
        BufferedInputStream bis=null;
        BufferedReader reader=null;
        String text="";
        try {
            fis=new FileInputStream(file);
            bis=new BufferedInputStream(fis);
            bis.mark(4);
            byte[] first3bytes=new byte[3];
            //   System.out.println("");
            //找到文档的前三个字节并自动判断文档类型。
            bis.read(first3bytes);
            bis.reset();
            if (first3bytes[0] == (byte) 0xEF && first3bytes[1] == (byte) 0xBB
                    && first3bytes[2] == (byte) 0xBF) {// utf-8

                reader = new BufferedReader(new InputStreamReader(bis, "utf-8"));
                Log.e("msc","utf-8");

            } else if (first3bytes[0] == (byte) 0xFF
                    && first3bytes[1] == (byte) 0xFE) {

                reader = new BufferedReader(
                        new InputStreamReader(bis, "unicode"));
                Log.e("msc","unicode");
            } else if (first3bytes[0] == (byte) 0xFE
                    && first3bytes[1] == (byte) 0xFF) {

                reader = new BufferedReader(new InputStreamReader(bis,
                        "utf-16be"));
                Log.e("msc","utf-16be");
            } else if (first3bytes[0] == (byte) 0xFF
                    && first3bytes[1] == (byte) 0xFF) {

                reader = new BufferedReader(new InputStreamReader(bis,
                        "utf-16le"));
                Log.e("msc","utf-16le");
            } else {

                reader = new BufferedReader(new InputStreamReader(bis, "GBK"));
                Log.e("msc","GBK");
            }
            String str = reader.readLine();

            while (str != null) {
                text = text + str + "/n";
                str = reader.readLine();
            }
            System.out.println("text"+text);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if (fis!=null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(bis!=null){
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return text;

    }
}