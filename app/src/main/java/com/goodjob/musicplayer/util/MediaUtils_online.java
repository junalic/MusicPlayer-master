package com.goodjob.musicplayer.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.goodjob.musicplayer.entity.Audio;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Author by sxjun.he, Date on 21-4-9.
 * PS: Not easy to write code, please indicate.
 */
public class MediaUtils_online {

    private static String TAG="msc";
    private static OkHttpClient client = new OkHttpClient();
    private static String murl="";
    private static Handler hanlder;
    private static List<Audio> mlist = new ArrayList<>();
    private static  Bitmap bitmap;
    public static void init_handle(Handler uiHandle)
    {
        hanlder=uiHandle;
    }

    public static void getAudioList(Context context) {
        final List<Audio> list = new ArrayList<>();

       OkHttpClient client = new OkHttpClient();

        // 获取在线音乐
            Log.e(TAG, "getOlineMusic: ");
            Request request = new Request.Builder()
                    .url("https://autumnfish.cn/top/song?type=96")
                    .build();
           // Log.e(TAG, "request.build: ");
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                            Log.e(TAG, "run: 网络错误");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.e(TAG, "onResponse");
                    String result = response.body().string();
                    try{
                        JSONObject obj = new JSONObject(result);
                        JSONArray songs = new JSONArray(obj.getString("data"));
                        for(int i=0; i<songs.length(); i++){
                            JSONObject song = songs.getJSONObject(i);
                            // Log.e(TAG, song.toString());
                            String id = song.getString("id");
                            //Log.e(TAG, "id = "+id);
                            String songurl1 = "https://autumnfish.cn/song/url?id=" + id ;
                            String songurl="";
                            while (songurl ==""){
                                getrealurl(songurl1);
                                Thread.sleep(30);
                                if(murl != "")
                                { songurl=murl;}
                            }
                            //Log.e(TAG, songurl);
                            murl="";
                            String name = song.getString("name");
                            int  mDuration= song.getInt("duration");
                            JSONObject album = song.getJSONObject("album");
                            JSONArray artists = song.getJSONArray("artists");
                            String singer1=artists.getString(0);
                            JSONObject singer2 = new JSONObject(singer1);
                            String singer=singer2.getString("name");
                            String pic = album.getString("blurPicUrl");
                            String mAlbum = album.getString("name");

                            //实例化一首音乐
                            if(songurl == "null") {
                                //Log.e(TAG, "fuck");
                            }
                            else
                            {
                                Audio audio=make(id,pic,name,singer , mAlbum ,songurl, i,mDuration,i, i ,true);
//                                Log.e(TAG, "audio");
//                                if(audio.getDuration() > 40 * 1000){
//                                Log.e("msc","mTitle= "+audio.getTitle());
//                                Log.e("msc","isMusic="+String.valueOf(audio.isMusic()));
//                                Log.e("msc","mArtist="+audio.getArtist());
//                                Log.e("msc","mAlbum="+audio.getAlbum());
//                                Log.e("msc","mPath="+audio.getPath());
//                                Log.e("msc","mId="+String.valueOf(audio.getId()));
//                                Log.e("msc","mDuration="+String.valueOf(audio.getDuration()));
//                                Log.e("msc","mArtistId="+String.valueOf(audio.getArtistId()));
//                                Log.e("msc","mAlbumId="+String.valueOf(audio.getAlbumId()));
//                                Log.e("msc","==================================");}
                                list.add(audio);

                                // list.add(new Audio(bundle));

                            }
                        }
//                        Message message = hanlder.obtainMessage();
//                        Log.e(TAG, "message: ");
//                        message.what = 60;
//                        message.obj = list;
//                        hanlder.sendMessage(message);
                        sendMyMessage(60,1,1,list);
                    }
                    catch (Exception e){}
                }

            });

    }


    public static void sendMyMessage(int what, int arg1, int arg2, Object objStr)
    {
        if(hanlder!=null)
            hanlder.obtainMessage(what, arg1, arg2, objStr).sendToTarget();
    }

    public static Bitmap getAlbumBitmapDrawable(Audio audio) {
        if (audio == null) {
            return null;
        }
        return getAlbumBitmapDrawable(audio.getDisplayName());
    }

    public static Bitmap getAlbumBitmapDrawable(final String url) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                URL imageurl = null;
                try {
                    imageurl = new URL(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                try {
                    HttpURLConnection conn = (HttpURLConnection)imageurl.openConnection();
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    bitmap = BitmapFactory.decodeStream(is);
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return bitmap;
        //return art != null ? BitmapFactory.decodeByteArray(art, 0, art.length) : null;
    }



    public static Audio make(String id, String albrmurl,String mTitle,String mArtist ,String mAlbum ,String mPath,int mId,int mDuration,int mArtistId,int mAlbumId ,boolean mIsMusic ){
        Audio audio = new Audio(new Bundle());
        audio.setMmId(id);
        audio.setDisplayName(albrmurl);
        audio.setmTitle(mTitle);
        audio.setmArtist(mArtist);
        audio.setmAlbum(mAlbum);
        audio.setmPath(mPath);
        audio.setmId(mId);
        audio.setmDuration(mDuration);
        audio.setmArtistId(mArtistId);
        audio.setmAlbumId(mAlbumId);
        audio.setmIsMusic(mIsMusic);
        return audio;
    }

    private static void getrealurl(String url) {

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                        Log.e(TAG, "run: 网络错误");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                murl = response.body().string();
                try{
                    JSONObject obj = new JSONObject(murl);
                    JSONArray data1 = new JSONArray(obj.getString("data"));
                    String singer1=data1.getString(0);
                    JSONObject singer2 = new JSONObject(singer1);
                    String singer=singer2.getString("url");
                    murl=singer;
                    //Log.e(TAG, "realurl = "+murl );
                }
                catch (Exception e){}
            }
        });
    }
    public static List<Audio> getMlist(){
        return mlist;
    }
}

