package com.goodjob.musicplayer.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.goodjob.musicplayer.entity.Audio;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Author by sxjun.he, Date on 21-4-16.
 * PS: Not easy to write code, please indicate.
 */

public class musicdbutil {
        Context context;
        public musicdbutil(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }

        /**
         * 保存
         * @param audio
         */
        public void saveObject(String id,Audio audio) {
            int myid= Integer.parseInt(id);
            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            try {
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(arrayOutputStream);
                objectOutputStream.writeObject(audio);
                objectOutputStream.flush();
                byte data[] = arrayOutputStream.toByteArray();
                objectOutputStream.close();
                arrayOutputStream.close();
               // Listdbhelper dbhelper = Listdbhelper.getInstens(context);
                Listdbhelper dbHelper = new Listdbhelper(context,"music_db",null,1);
                SQLiteDatabase database = dbHelper.getWritableDatabase();
                database.execSQL("insert into classtable (id,classtabledata) values(?,?)", new Object[] { myid,data });
                database.close();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public List<Audio> getObject() {
            Audio student = null;
            List<Audio> Audiolist= new ArrayList<Audio>();
            Listdbhelper dbHelper = new Listdbhelper(context,"music_db",null,1);
            SQLiteDatabase database = dbHelper.getWritableDatabase();
//            MyDbHelper dbhelper = MyDbHelper.getInstens(context);
//            SQLiteDatabase database = dbhelper.getReadableDatabase();
            Cursor cursor = database.rawQuery("select * from classtable", null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    byte data[] = cursor.getBlob(cursor.getColumnIndex("classtabledata"));
                    ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(data);
                    try {
                        ObjectInputStream inputStream = new ObjectInputStream(arrayInputStream);
                        student = (Audio) inputStream.readObject();
                        Audiolist.add(student);
                        inputStream.close();
                        arrayInputStream.close();
                        //break;//这里为了测试就取一个数据
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            }
            return Audiolist;

        }

    public void  deletedb(String id){
        Listdbhelper dbHelper = new Listdbhelper(context,"music_db",null,1);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
//            MyDbHelper dbhelper = MyDbHelper.getInstens(context);
//            SQLiteDatabase database = dbhelper.getReadableDatabase();
        //database.execSQL("delete from classtabledata where id=?", new Object[] { myid });
        database.delete("classtable", "id = ?", new String[]{ id });
        database.close();



        }
    }