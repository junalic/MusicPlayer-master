package com.goodjob.musicplayer.activity;

import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.arialyy.aria.core.Aria;
import com.arialyy.aria.core.download.DownloadTask;
import com.goodjob.musicplayer.R;
import com.goodjob.musicplayer.util.HorizontalProgressBarWithNumber;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Author by sxjun.he, Date on 21-4-13.
 * PS: Not easy to write code, please indicate.
 */
public class download extends AppCompatActivity {
        private static String DOWNLOAD_URL ="http://m701.music.126.net/20210413111211/f20ecbd284ca71b2900d8fa47ca9cd04/jdymusic/obj/wo3DlMOGwrbDjj7DisKw/8332154021/d677/0779/f65e/9ad873367368245ca10e1c2929a7f45e.mp3";
        String mfile;
        @Bind(R.id.progressBar)
        HorizontalProgressBarWithNumber mPb;
        //@Bind(R.id.start) Button mStart;
        //@Bind(R.id.stop) Button mStop;
        //@Bind(R.id.cancel) Button mCancel;
        @Bind(R.id.size)
        TextView mSize;
        @Bind(R.id.speed) TextView mSpeed;

        @Override protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_download);
            ButterKnife.bind(this);
        }


    public void scanFileToMedia(final String url) {
        new Thread(new Runnable() {
            public void run() {
                MediaScannerConnection.scanFile(download.this, new String[] {url}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                                Log.d("msc", "scan completed : file = " + url);
                            }
                        });
            }

        }).start();
    }

        @OnClick({ R.id.start, R.id.stop, R.id.cancel }) public void onClick(View view) {
            switch (view.getId()) {
                case R.id.start:
                    String murl=getIntent().getStringExtra("url");
                    String name=getIntent().getStringExtra("name");
                    File file=new File(Environment.getExternalStorageDirectory().getPath() + "/music/"+name+".mp3");
                    mfile=file.getPath();
                    DOWNLOAD_URL=murl;
                    Aria.download(this)
                            .load(murl)
                            .setDownloadPath(file.getPath())
                            .start();

                    //MediaScannerConnection.scanFile(this, new String[] {file.getAbsolutePath()},null, null);
                    Log.e("msc",Environment.getExternalStorageDirectory().getPath() + "/music/");
                    break;
                case R.id.stop:
                    Aria.download(this).load(DOWNLOAD_URL).pause();
                    break;
                case R.id.cancel:
                    Aria.download(this).load(DOWNLOAD_URL).cancel();
                    break;
            }
        }

        @Override protected void onResume() {
            super.onResume();
            Aria.download(this).addSchedulerListener(new MySchedulerListener());
        }

        private class MySchedulerListener extends Aria.DownloadSchedulerListener {

            @Override public void onTaskStart(DownloadTask task) {
                mSize.setText(task.getConvertFileSize());
            }

            @Override public void onTaskStop(DownloadTask task) {
                Toast.makeText(download.this, "停止下载", Toast.LENGTH_SHORT).show();
            }

            @Override public void onTaskCancel(DownloadTask task) {
                Toast.makeText(download.this, "取消下载", Toast.LENGTH_SHORT).show();
                mSize.setText("0m");
                mSpeed.setText("");
                mPb.setProgress(0);
            }

            @Override public void onTaskFail(DownloadTask task) {
                Toast.makeText(download.this, "下载失败", Toast.LENGTH_SHORT).show();
            }

            @Override public void onTaskComplete(DownloadTask task) {
                scanFileToMedia(mfile);
                Toast.makeText(download.this, "下载完成", Toast.LENGTH_SHORT).show();
            }

            @Override public void onTaskRunning(DownloadTask task) {
                //使用转换单位后的速度，需要在aria_config.xml配置文件中将单位转换开关打开
                //https://github.com/AriaLyy/Aria#配置文件设置参数
                mSpeed.setText(task.getConvertSpeed());
                mPb.setProgress(task.getPercent());
            }
        }
    }

