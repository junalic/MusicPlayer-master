package com.goodjob.musicplayer.activity;

/**
 * Author by sxjun.he, Date on 21-4-9.
 * PS: Not easy to write code, please indicate.
 */
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.PermissionChecker;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.goodjob.musicplayer.R;
import com.goodjob.musicplayer.adapter.AudioListAdapter;
import com.goodjob.musicplayer.adapter.ViewPagerAdapter;
import com.goodjob.musicplayer.entity.Audio;
import com.goodjob.musicplayer.entity.AudioItem;
import com.goodjob.musicplayer.service.AudioPlayService;
import com.goodjob.musicplayer.util.AudioList;
import com.goodjob.musicplayer.util.AudioList_online;
import com.goodjob.musicplayer.util.AudioToAudioItem;
import com.goodjob.musicplayer.util.MediaUtils;
import com.goodjob.musicplayer.util.MediaUtils_online;

import net.sourceforge.pinyin4j.PinyinHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class onlineListActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String SHARED_PREFERENCES_NAME = "music_player_online";
    private static final String TAG = "msc";


    private List<BaseAdapter> mAdapterList;

    private TextView mBarTitle;
    private TextView mBarArtist;
    private ImageView mBarAlbum;
    private ImageButton mBarPauseButton;
    private ImageButton mBarNextButton;
    private View mBarPauseBackground;
    private PagerTitleStrip mPaperTitleStrip;
    private  List<Audio> mlist = new ArrayList<>();
    private BroadcastReceiver mEventReceiver;
    private AudioList_online mAudioList_online;
    private List<List<AudioItem>> mListOfAudioItemList;
    private int mPlayingIndex = -1;
    private int flag = 0;
    private List<Integer> mShuffleIndex;

    private int mLastPlay = -1;
    private int mLastIndex = -1;
    Handler mainHanlder;
    private boolean mIsPlaying = false;
    private boolean mIsShuffle = false;

    private int mLoopWay;

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("msc", "onlineListActivity onCreate: ");
        setContentView(R.layout.activity_online);
        readStatus();
        View barView = findViewById(R.id.bar_online);

        mainHanlder = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                //Log.e("msc", String.valueOf(msg.what));
                switch (msg.what){
                    case 60:

                        mlist  = (List<Audio>) msg.obj;
                        chuli(mlist);
                        break;
                }
            }
        };
        MediaUtils_online.init_handle(mainHanlder);
        mBarTitle = (TextView) barView.findViewById(R.id.title_online);

        mBarArtist = (TextView) barView.findViewById(R.id.artist_online);
        mBarAlbum = (ImageView) barView.findViewById(R.id.album_online);
        mBarPauseButton = (ImageButton) barView.findViewById(R.id.home_pauseButton_online);
        mBarNextButton = (ImageButton) barView.findViewById(R.id.home_nextButton_online);
        mBarPauseBackground = barView.findViewById(R.id.homebar_background_online);
        mPaperTitleStrip = (PagerTitleStrip) findViewById(R.id.title_strip_online);

        //设置tab栏字体
        mPaperTitleStrip.setTextColor(Color.rgb(255, 255, 255));
        mBarTitle.setHorizontallyScrolling(true);
        mBarTitle.setSelected(true);
        mBarArtist.setHorizontallyScrolling(true);
        mBarArtist.setSelected(true);

        enableButton(false, true);

        // 弹出播放器界面
        barView.setOnClickListener(this);

        // 播放条暂停按钮事件监听器
        mBarPauseButton.setOnClickListener(this);

        // 播放条暂停按钮背景事件监听器
        mBarPauseBackground.setOnClickListener(this);

        // 播放条下一首事件监听器
        mBarNextButton.setOnClickListener(this);

        // 事件广播接受器
        LocalBroadcastManager.getInstance(this).registerReceiver(mEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String event = intent.getStringExtra(AudioPlayService.EVENT_KEY);
                if (event == null) {
                    return;
                }
                switch (event) {
//                    case AudioPlayService.FINISHED_EVENT:
//                        musicChange(true, false);
//                        if (mPlayingIndex >= 0 && mPlayingIndex < mAdapterList.size()) {
//                            mAdapterList.get(mPlayingIndex).notifyDataSetChanged();
//                        }
//                        break;
//                    case AudioPlayService.NEXT_EVENT:
//                        musicChange(true, true);
//                        if (mPlayingIndex >= 0 && mPlayingIndex < mAdapterList.size()) {
//                            mAdapterList.get(mPlayingIndex).notifyDataSetChanged();
//                        }
//                        break;
//                    case AudioPlayService.PREVIOUS_EVENT:
//                        musicChange(false, true);
//                        if (mPlayingIndex >= 0 && mPlayingIndex < mAdapterList.size()) {
//                            mAdapterList.get(mPlayingIndex).notifyDataSetChanged();
//                        }
//                        break;
                    case AudioPlayService.PLAY_EVENT_ONLINE:
                        boolean isPlay = intent.getBooleanExtra(AudioPlayService.AUDIO_PLAY_NOW_BOOL, false);
                        if (isPlay) {
                            mBarPauseButton.setImageResource(R.drawable.pause_light);
                            mIsPlaying = true;
                        } else {
                            mBarPauseButton.setImageResource(R.drawable.play_light);
                            mIsPlaying = false;
                        }
                        enableButton(true);
                        break;
//                    case AudioPlayService.PAUSE_EVENT:
//                        mBarPauseButton.setImageResource(R.drawable.play_light);
//                        mIsPlaying = false;
//                        enableButton(true);
//                        break;
//                    case AudioPlayService.REPLAY_EVENT:
//                        mBarPauseButton.setImageResource(R.drawable.pause_light);
//                        mIsPlaying = true;
//                        enableButton(true);
//                        break;
//                    case AudioPlayService.LIST_ORDER_EVENT:
//                        mIsShuffle = intent.getBooleanExtra(AudioPlayService.LIST_SHUFFLE_BOOL, true);
//                        if (mIsShuffle) {
//                            shuffleAudioIndex(mListOfAudioItemList.get(mPlayingIndex), mLastPlay);
//                            mLastIndex = 0;
//                        }
//                        saveStatus();
//                        break;
//                    case AudioPlayService.CHANGE_LOOP_EVENT:
//                        mLoopWay = intent.getIntExtra(
//                                AudioPlayService.LOOP_WAY_INT, AudioPlayService.LIST_NOT_LOOP);
//                        saveStatus();
//                        break;
                }
            }
        }, new IntentFilter(AudioPlayService.BROADCAST_EVENT_FILTER));
            init();

    }
     private void chuli(List<Audio> chulilist){

         //List<Audio> ssss= mAudioList_online.getAudioList1(chulilist);
         //List<Audio> newList = new ArrayList<>(getAudioList(context));
         List<Audio> ssss=new ArrayList<>(mAudioList_online.getAudioList1(chulilist));
         //Log.e("msc", "chuli: ssss");
        // 标题
        final String[] titles = new String[] {
                "Audio", "Artist", "Album"
        };

        // 排序比较器
        Comparator[] cmps = new Comparator[] {
                new Comparator<Audio>() {
                    @Override
                    public int compare(Audio o1, Audio o2) {
                        return getPinyinString(o1.getTitle())
                                .compareToIgnoreCase(getPinyinString(o2.getTitle()));
                    }
                },
                new Comparator<Audio>() {
                    @Override
                    public int compare(Audio o1, Audio o2) {
                        int res = getPinyinString(o1.getArtist())
                                .compareToIgnoreCase(getPinyinString(o2.getArtist()));
                        if (res != 0) {
                            return res;
                        }
                        return o1.getArtistId() - o2.getArtistId();
                    }
                },
                new Comparator<Audio>() {
                    @Override
                    public int compare(Audio o1, Audio o2) {
                        int res = getPinyinString(o1.getAlbum())
                                .compareToIgnoreCase(getPinyinString(o2.getAlbum()));
                        if (res != 0) {
                            return res;
                        }
                        return o1.getAlbumId() - o2.getAlbumId();
                    }
                }
        };
        // 转换
        AudioToAudioItem[] trans = new AudioToAudioItem[] {
                new AudioToAudioItem() {
                    @Override
                    public AudioItem apply(Audio audio) {
                        AudioItem audioItem = new AudioItem(audio);
                        String title = getPinyinString(audio.getTitle()).toUpperCase();
                        audioItem.setClassificationId(title.length() > 0 ? title.charAt(0) : -1);
                        audioItem.setClassificationName(title.length() > 0 ? title.charAt(0) + "" : "");
                        return audioItem;
                    }
                },
                new AudioToAudioItem() {
                    @Override
                    public AudioItem apply(Audio audio) {
                        AudioItem audioItem = new AudioItem(audio);
                        audioItem.setClassificationId(audio.getArtistId());
                        audioItem.setClassificationName(audio.getArtist());
                        return audioItem;
                    }
                },
                new AudioToAudioItem() {
                    @Override
                    public AudioItem apply(Audio audio) {
                        AudioItem audioItem = new AudioItem(audio);
                        audioItem.setClassificationId(audio.getAlbumId());
                        audioItem.setClassificationName(audio.getAlbum());
                        return audioItem;
                    }
                }
        };

        List<View> viewList = new ArrayList<>();
        List<String> titleList = new ArrayList<>();

        mAdapterList = new ArrayList<>();
        mListOfAudioItemList = new ArrayList<>();
         for (int i = 0; i < titles.length; ++i) {
            // List<Audio> list = AudioList.getAudioList(this, cmps[i]);
             Collections.sort(ssss, cmps[i]);
              List<Audio> list = ssss;
             List<AudioItem> itemList = new ArrayList<>();
             for (Audio audio : list) {
                 itemList.add(trans[i].apply(audio));
             }

             mListOfAudioItemList.add(itemList);
             final AudioListAdapter adapter = new AudioListAdapter(this, R.layout.list_music, itemList);

             //列表项中更多按钮的点击事件
             final int finalI = i;
             adapter.setOnMoreButtonListener(new AudioListAdapter.onMoreButtonListener() {
                 @Override
                 public void onClick(final int ii) {
                     mPlayingIndex = finalI;
                     List<AudioItem> list = mListOfAudioItemList.get(mPlayingIndex);
                     AudioItem audioItem = list.get(ii);
                     final Audio music = audioItem.getAudio();
                     //final Audio music = onlinemusic_list.get(i);
                     final String[] items = new String[] {"下载", "下载管理", "删除"};
                     AlertDialog.Builder builder = new AlertDialog.Builder(onlineListActivity.this);
                     builder.setTitle(music.getTitle()+"-"+music.getArtist());

                     builder.setItems(items, new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             switch (which){
                                 case 0:
                                     Intent intent1 = new Intent(onlineListActivity.this, download.class);
                                     intent1.putExtra("url",music.getPath());
                                     intent1.putExtra("name",music.getTitle());
                                     startActivity(intent1);
                                     break;
                                 case 1:
                                     Intent intent = new Intent(onlineListActivity.this, download.class);
                                     startActivity(intent);
                                     break;
                                 case 2:
                                     //从列表中删除
//                                     onlinemusic_list.remove(i);
//                                     adapter.notifyDataSetChanged();
//                                     musicCountView.setText("播放全部(共"+onlinemusic_list.size()+"首)");
                                     break;
                             }
                         }
                     });
                     builder.create().show();
                 }
             });

             mAdapterList.add(adapter);
             ListView listView = new ListView(this);
             listView.setAdapter(adapter);
             final int index = i;
             listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                 @Override
                 public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                     mPlayingIndex = index;
                     Log.e("msc", "onItemClick: "+position);
                     playAudio(position, true, true, false);
                     adapter.notifyDataSetChanged();

                 }
             });

             viewList.add(listView);
             titleList.add(titles[i]);
         }


        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager_online);
        viewPager.setOffscreenPageLimit(viewList.size() - 1);
        viewPager.setAdapter(new ViewPagerAdapter(titleList, viewList));


    }



    private static String getPinyinString(String str) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < str.length(); ++i) {
            char ch = str.charAt(i);
            String[] pinyin = PinyinHelper.toHanyuPinyinStringArray(ch);
            if (pinyin != null) {
                builder.append(pinyin[0]);
            } else {
                builder.append(ch);
            }
        }
        return builder.toString();
    }

    private void saveStatus() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(AudioPlayService.LOOP_WAY_INT, mLoopWay);
        editor.putBoolean(AudioPlayService.LIST_SHUFFLE_BOOL, mIsShuffle);
        editor.apply();
    }

    private void readStatus() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        mLoopWay = sharedPreferences.getInt(AudioPlayService.LOOP_WAY_INT, AudioPlayService.LIST_NOT_LOOP);
        mIsShuffle = sharedPreferences.getBoolean(AudioPlayService.LIST_SHUFFLE_BOOL, false);
    }

    private Intent getAudioIntent(Audio audio) {
        Intent intent = new Intent();
        intent.putExtra(AudioPlayService.AUDIO_PATH_STR, audio.getPath());
        intent.putExtra(AudioPlayService.AUDIO_onlinePATH_STR, audio.getDisplayName());
        intent.putExtra(AudioPlayService.AUDIO_TITLE_STR, audio.getTitle());
        intent.putExtra(AudioPlayService.AUDIO_ARTIST_STR, audio.getArtist());
        intent.putExtra(AudioPlayService.AUDIO_ALBUM_ID_INT, audio.getAlbumId());
        intent.putExtra(AudioPlayService.AUDIO_DURATION_INT, audio.getDuration());
        intent.putExtra(AudioPlayService.AUDIO_CURRENT_INT, 0);
        intent.putExtra("mmId", audio.getMmId());
        intent.putExtra("flag", 1);
        return intent;
    }

    private void playAudio(int position) {
        playAudio(position, true, false, false);
    }

    /**
     *
     * @param position 在原始音乐列表的位置
     * @param shuffle  是否再次打乱顺序
     */
    private void playAudio(int position, boolean start, boolean shuffle, boolean forced) {
        Log.e("msc", "playAudio: ");
        if (forced || position != mLastPlay) {
            List<AudioItem> list = mListOfAudioItemList.get(mPlayingIndex);

            AudioItem audioItem = list.get(position);

            Audio audio = audioItem.getAudio();
            if (shuffle) {
                shuffleAudioIndex(list, position);
                mLastIndex = 0;
            }

//                                            Log.e("msc","mTitle= "+audio.getTitle());
//                                Log.e("msc","isMusic="+String.valueOf(audio.isMusic()));
//                                Log.e("msc","mArtist="+audio.getArtist());
//                                Log.e("msc","mAlbum="+audio.getAlbum());
//                                Log.e("msc","mPath="+audio.getPath());
//                                Log.e("msc","mId="+String.valueOf(audio.getId()));
//                                Log.e("msc","mDuration="+String.valueOf(audio.getDuration()));
//                                Log.e("msc","mArtistId="+String.valueOf(audio.getArtistId()));
//                                Log.e("msc","mAlbumId="+String.valueOf(audio.getAlbumId()));
//                                Log.e("msc","==================================");
            Intent serviceIntent1 = getAudioIntent(audio);
            serviceIntent1.putExtra(AudioPlayService.ACTION_KEY, AudioPlayService.PLAY_ACTION);

            serviceIntent1.putExtra(AudioPlayService.AUDIO_PLAY_NOW_BOOL, start);


            serviceIntent1.setClass(this, AudioPlayService.class);

            mLastPlay = position;

            mBarTitle.setText(audio.getTitle());

            mBarArtist.setText(audio.getArtist());

            Bitmap bitmap = MediaUtils_online.getAlbumBitmapDrawable(audio);
            if (bitmap != null) {
                mBarAlbum.setImageDrawable(new BitmapDrawable(getResources(), bitmap));
            } else {
                mBarAlbum.setImageResource(R.drawable.no_album);
            }
            enableButton(false);
            startService(serviceIntent1);
            Log.e("msc", "playAudio: 4");
        }
    }

    /**
     * 把indexList乱序后将值=playIndex的项交换到开头
     * @param playIndex
     */
    private void shuffleAudioIndex(List<? extends Object> audioList, int playIndex) {
        if (mShuffleIndex == null) {
            mShuffleIndex = new ArrayList<>();
        }
        if (mShuffleIndex.size() != audioList.size()) {
            mShuffleIndex.clear();
            for (int i = 0; i < audioList.size(); ++i) {
                mShuffleIndex.add(i);
            }
        }
        Collections.shuffle(mShuffleIndex);
        for (int i = 0; i < mShuffleIndex.size(); ++i) {
            if (mShuffleIndex.get(i) == playIndex) {
                Collections.swap(mShuffleIndex, i, 0);
                break;
            }
        }
    }

    /**
     * 歌曲切换
     * @param next      是否为下一首
     * @param fromUser  是否来自用户的动作
     */
    private void musicChange(boolean next, boolean fromUser) {
        if (mLoopWay == AudioPlayService.AUDIO_REPEAT && !fromUser) {
            playAudio(mLastPlay, true, false, true);
        } else {
            int index;
            if (mIsShuffle) {
                int listSize = mShuffleIndex.size();
                if (next) {
                    index = mShuffleIndex.get(mLastIndex = (mLastIndex + 1) % listSize);
                } else {
                    index = mShuffleIndex.get(
                            mLastIndex = (mLastIndex - 1 + listSize) % listSize);
                }
                mLastIndex = index;
            } else {
                int listSize = mListOfAudioItemList.get(mPlayingIndex).size();
                if (next) {
                    index = (mLastPlay + 1) % listSize;
                } else {
                    index = (mLastPlay - 1 + listSize) % listSize;
                }
            }
            if (index == 0 && next && !fromUser && mLoopWay == AudioPlayService.LIST_NOT_LOOP) {
                playAudio(index, false, mIsShuffle, true);
            } else {
                playAudio(index, mIsPlaying, true, true);
            }
        }
    }

    /**
     * 初始化列表
     */
    private void init() {
        MediaUtils_online.getAudioList(this);

    }

    private void pause() {

        if (mLastPlay >= 0) {
            Intent intent = new Intent(onlineListActivity.this, AudioPlayService.class);
            if (mIsPlaying) {
                intent.putExtra(AudioPlayService.ACTION_KEY, AudioPlayService.PAUSE_ACTION);
            } else {
                intent.putExtra(AudioPlayService.ACTION_KEY, AudioPlayService.REPLAY_ACTION);
            }
            enableButton(false);
            startService(intent);
        }
    }

    private void enableButton(boolean enable) {
        enableButton(enable, false);
    }

    private void enableButton(boolean enable, boolean grey) {
        mBarPauseButton.setEnabled(enable);
        mBarPauseBackground.setEnabled(enable);
        mBarNextButton.setEnabled(enable);

        if (grey && !enable) {
            mBarPauseBackground.setBackgroundResource(R.drawable.shadowed_circle_grey);
        } else {
            mBarPauseBackground.setBackgroundResource(R.drawable.shadowed_circle_red);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, AudioPlayService.class));
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mEventReceiver);
    }



    @Override
    public void finish() {
        moveTaskToBack(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // 弹出播放器界面
            case R.id.bar_online:
                Log.e(TAG, "onClick: bar");
                if (mPlayingIndex >= 0 && mPlayingIndex < mListOfAudioItemList.size()) {
                    List<AudioItem> audioItemList = mListOfAudioItemList.get(mPlayingIndex);
                    if (mLastPlay >= 0 && mLastPlay < audioItemList.size()) {
                        Intent intent = getAudioIntent(audioItemList.get(mLastPlay).getAudio());
                        intent.setClass(onlineListActivity.this, PlayerActivity.class);
                        intent.putExtra(AudioPlayService.AUDIO_IS_PLAYING_BOOL, mIsPlaying);
                        intent.putExtra(AudioPlayService.LIST_SHUFFLE_BOOL, mIsShuffle);
                        intent.putExtra(AudioPlayService.LOOP_WAY_INT, mLoopWay);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_top);
                    }
                }
                break;
            // 暂停按钮
            case R.id.home_pauseButton_online: case R.id.homebar_background_online:
                Log.e(TAG, "onClick: home_pauseButton");
                pause();
                break;
            // 下一首
            case R.id.home_nextButton_online:
                Log.e(TAG, "onClick: home_nextButton");
                musicChange(true, true);
                break;

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (mIsPlaying) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
//                    AlertDialog dialog = builder.setTitle("音乐正在播放")
//                            .setPositiveButton("后台播放", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    onlineListActivity.this.finish();
//                                }
//                            })
//                            .setNegativeButton("退出", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    onlineListActivity.super.finish();
//                                }
//                            }).create();
                    onlineListActivity.super.finish();
//                    dialog.show();
                } else {
                    super.finish();
                }
                break;
        }
        return super.onKeyDown(keyCode, event);

    }


}
